thonny (4.1.7-1) unstable; urgency=medium

  * Team upload
  * New upstream version 4.1.7
  * d/thonny.lintian-overrides: Drop file, unused override
  * d/s/lintian-overrides: Drop file, override is not for a false positive
  * d/control: Add Rules-Requires-Root: no
  * d/control: Switch to debhelper-compat
  * d/rules: Don't install .DS_Store files

 -- Samuel Henrique <samueloph@debian.org>  Sat, 08 Feb 2025 19:01:02 +0000

thonny (4.1.3-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version.
  * Bump Standards-Version to 4.6.2 (no changes needed).
  * Bump copyright year in d/copyright for upstream.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Thu, 28 Sep 2023 23:40:33 +0200

thonny (4.0.1-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version.
  * Bump Standards-Version to 4.6.1 (no changes needed).
  * Bump versions of some dependencies
  * Add python3-wheel dependency

  [ Dominik George ]
  * Bump copyright year in d/copyright for upstream.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Sat, 5 Nov 2022 11:40:33 +0200

thonny (3.3.14-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version.
  * Bump Standards-Version to 4.6.0 (no changes needed).

  [ Dominik George ]
  * Add package version constraints to d/control.
  * Add lintian override for link from usr/lib to usr/share.
  * Bump compat level to 13.

 -- Dominik George <natureshadow@debian.org>  Mon, 13 Sep 2021 10:10:33 +0200

thonny (3.2.7-1) unstable; urgency=medium

  * New upstream version (Closes: #950884).
  * Bump Standards-Version (no changes needed).

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Mon, 10 Feb 2020 22:12:16 +0200

thonny (3.2.1-1) unstable; urgency=medium

  * New upstream version.
  * Change pylint3 dependency to pylint. (Closes: #944831)

 -- Dominik George <natureshadow@debian.org>  Thu, 21 Nov 2019 11:14:41 +0100

thonny (3.2.0-1) unstable; urgency=medium

  * New upstream version.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Mon, 12 Aug 2019 20:12:16 +0200

thonny (3.1.2-1) unstable; urgency=medium

  * New upstream version.

 -- Dominik George <natureshadow@debian.org>  Fri, 22 Feb 2019 20:12:16 +0100

thonny (3.1.1-1) unstable; urgency=medium

  * New upstream version.
  * Depend on asttokens now it is in Debian.

 -- Dominik George <natureshadow@debian.org>  Tue, 12 Feb 2019 12:55:27 +0100

thonny (3.1.0-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version.

  [ Dominik George ]
  * Add Recommends and Suggests from Python helper.
  * Remove duplicate explicit dependencies.

 -- Dominik George <natureshadow@debian.org>  Wed, 30 Jan 2019 13:23:59 +0100

thonny (3.0.8-1) unstable; urgency=medium

  * New upstream version.

 -- Dominik George <natureshadow@debian.org>  Sat, 01 Dec 2018 12:23:58 +0100

thonny (3.0.5-1) unstable; urgency=medium

  * New upstream version 3.0.5

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Sat, 27 Oct 2018 19:13:21 +0200

thonny (3.0.4-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version 3.0.4
  * Add required dependencies:
    - python3-docutils
    - python3-serial
    - python3-pyperclip
    - pylint3
    - mypy
  * Add recommended dependencies:
    - zenity
    - xsel
  * Add suggested dependency: python3-distro
  * Update Thonny homepage (https instead of http)
  * Point docs to README.rst

  [ Dominik George ]
  * Fix version requirement for jedi.
  * Remove code for using help.rst as README.
  * Rename all binary-package related files to thonny.*.

 -- Dominik George <natureshadow@debian.org>  Fri, 26 Oct 2018 18:48:24 +0200

thonny (2.1.22-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * Depend on pkg-resources. (Closes: #904771)

  [ Aivar Annamaa ]
  * New upstream version 2.1.22
  * Add python3-pkg-resources as dependency (by Mike Gabriel)
  * Update debian/watch to ignore beta versions

  [ Dominik George ]
  * Update my mail address as Uploader.
  * Update dependency to jedi to >= 0.10.
  * Fix installation of resources after Python 3.7 was enabled.
  * Bump Standards-Version to 4.2.1.
    + No changes needed.

 -- Dominik George <natureshadow@debian.org>  Thu, 27 Sep 2018 10:56:42 +0200

thonny (2.1.17-1) unstable; urgency=medium

  [ Dominik George ]

  * Move to Salsa.

  [ Aivar Annamaa ]

  * New upstream version 2.1.17

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Fri, 23 Mar 2018 15:26:21 +0200

thonny (2.1.16-3) unstable; urgency=medium

  * Install icons to correct location.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Wed, 14 Feb 2018 20:36:15 +0200

thonny (2.1.16-2) unstable; urgency=medium

  * Install desktop and metainfo files to correct location.
  * Bump Standards-Version and compat level (no changes needed).
  * Rename lintian tag debian-watch-does-not-check-gpg-signature.
  * Use HTTPS URL for copyright format.

 -- Dominik George <nik@naturalnet.de>  Tue, 13 Feb 2018 15:17:43 +0100

thonny (2.1.16-1) unstable; urgency=medium

  * Initial release (Closes: #857042)

 -- Dominik George <nik@naturalnet.de>  Sun, 03 Dec 2017 18:00:55 +0100
